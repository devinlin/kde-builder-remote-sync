FROM fedora:41

WORKDIR /kde-builder

ENV PATH="/root/.local/bin:$PATH"

# Setup kde-builder
RUN export PATH=/root/.local/bin:$PATH && \
    curl 'https://invent.kde.org/sdk/kde-builder/-/raw/master/scripts/initial_setup.sh?ref_type=heads' > initial_setup.sh && \
    yes | bash initial_setup.sh && \
    yes | kde-builder --generate-config && \
    yes | kde-builder --install-distro-packages

# Useful dependencies
RUN sudo dnf install -y gpgme \
                        gnupg2-smime
