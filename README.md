# kde-builder-remote-sync

Utility to host a `kde-builder` "server" that can build a snapshot of the `~/kde/usr` folder, and have it synced to clients.

This currently assumes the client has Fedora 40.

#### Build container

```bash
$ docker build -t kde-builder-fedora:latest .
```

#### Run Container

This bind mounts the kde folder to `/opt/kde`.

```bash
$ docker run --name kde-builder --rm -v /opt/kde:/root/kde kde-builder-fedora:latest command
```

Replace `command` with a `kde-builder` command, such as `kde-builder plasma-mobile`.

For example:

```bash
$ docker run --name kde-builder --rm -v /opt/kde:/root/kde kde-builder-fedora:latest kde-builder plasma-mobile --no-install-login-session
```

**Note:** It is highly recommended to use the `--no-install-login-session` option, as it will otherwise fail.

#### Syncing the server to client

```bash
$ rsync --info=progress2 -a user@hostname:/opt/kde/usr ~/kde/usr
```
